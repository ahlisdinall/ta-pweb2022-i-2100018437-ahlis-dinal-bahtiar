<?php
	echo "<h3>PROGRAM 9.1 : </h3>";
	$gaji = 1000000;
	$pajak = 0.1;
	$thp = $gaji - ($gaji*$pajak);

	echo "Gaji sebelum pajak = Rp. $gaji <br>";
	echo "Gaji yang dibawa pulang = Rp. $thp";

	echo "<h3>PROGRAM 9.2 : </h3>";
	$a = 5;
	$b = 4;

	echo "$a == $b : ". ($a == $b);
	echo "<br>$a != $b : ". ($a	!= $b);
	echo "<br>$a > $b : ". ($a	> $b);
	echo "<br>$a < $b : ". ($a	< $b);
	echo "<br>($a == $b) && ($a > $b) : ". (($a == $b) && ($a > $b));
	echo "<br>($a == $b) || ($a > $b) : ". (($a == $b) || ($a > $b));
	echo "<br>($a != $b) && ($a > $b) : ". (($a != $b) && ($a > $b));
	echo "<br>($a != $b) || ($a > $b) : ". (($a != $b) || ($a > $b));
?>